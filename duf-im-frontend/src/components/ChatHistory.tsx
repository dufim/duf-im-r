import React from "react";
import Message from "../model/Message";
import { Grid } from "@material-ui/core";
import MessageComponent from "./MessageComponent";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";

//the messages
export interface MessageHistoryProps {
  history: Message[];
}

//css
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    message: {
      [theme.breakpoints.down("xs")]: {
        maxWidth: "100%"
      },
      [theme.breakpoints.up("sm")]: {
        maxWidth: "90%"
      },
      [theme.breakpoints.between("md", "xl")]: {
        maxWidth: "80%"
      }
    }
  })
);

/**
 * A wrapper to generate MessageComponents for given messages.
 * Messages by the activeUser are always on the right, while
 * messages by others are on the left.
 *
 * @export
 * @param {MessageHistoryProps} { history }
 * @returns a list of MessageComponents
 */
export default function MessageHistory({ history }: MessageHistoryProps) {
  const classes = useStyles();
  return history ? (
    <>
      {history.map((message, index) => {
        return (
          <Grid
            container
            justify={message.owner.master ? "flex-end" : "flex-start"}
            key={message.eventId + index}
          >
            <Grid
              item
              className={classes.message}
              key={message.eventId + index + 0.1}
            >
              <MessageComponent message={message} key={message.eventId} />
            </Grid>
          </Grid>
        );
      })}
    </>
  ) : (
    <></>
  );
}
