import React, { ReactNode } from "react";
import {
  createStyles,
  makeStyles,
  Theme,
  fade,
  useTheme
} from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ExitToApp from "@material-ui/icons/ExitToApp";
import Divider from "@material-ui/core/Divider";
import {
  Drawer,
  ListItem,
  List,
  CssBaseline,
  Grid,
  Collapse,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  Avatar,
  ListItemSecondaryAction,
  ListItemAvatar,
  Popper,
  Fade,
  Paper
} from "@material-ui/core";
import { green, red } from "@material-ui/core/colors";
import clsx from "clsx";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Chat from "@material-ui/icons/Chat";
import PersonAdd from "@material-ui/icons/PersonAdd";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import { RoomSummary } from "../model/Room";
import { stringToColor } from "../utils/utils";
import Backend from "../scripts/backend";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

const drawerWidth = 240;

/**
 * Has various status indicators and functions provided by the ChatApp component.
 * The other variables are used to construct the SideBar.
 *
 * @export
 * @interface AuthAppBarProps
 */
export interface AuthAppBarProps {
  loggedIn: boolean;
  loadingMessages: boolean;
  handleLogout: () => void;
  children?: ReactNode;
  rooms: RoomSummary[];
  activeRoom: RoomSummary | undefined;
  handleRoomChange: (room: RoomSummary) => void;
  handleLeaveRoom: (room: RoomSummary) => void;
  handleInvite: (roomId: string, server: string, username: string) => void;
  createRoom: (roomName: string, userId: string) => void;
}

/**
 * Holds a permanent AppBar at the top with a logout button when logged in.
 * Also has a permanent SideBar which can display various Dialogs.
 *
 * @export
 * @param {AuthAppBarProps} {
 *   handleLogout,
 *   handleRoomChange,
 *   handleLeaveRoom,
 *   handleInvite,
 *   children,
 *   rooms,
 *   activeRoom,
 *   createRoom,
 *   loggedIn,
 *   loadingMessages
 * }
 * @returns
 */
export default function AuthAppBar({
  handleLogout,
  handleRoomChange,
  handleLeaveRoom,
  handleInvite,
  children,
  rooms,
  activeRoom,
  createRoom,
  loggedIn,
  loadingMessages
}: AuthAppBarProps) {
  const classes = useStyles();
  const theme = useTheme();

  //hooks for opening and closing dialogs or the drawer or poppers. Also holds textinput values.
  const [drawerOpen, setOpen] = React.useState(true);
  const [roomsOpen, toggleRooms] = React.useState<boolean>(true);
  const [dialogOpen, toggleDialog] = React.useState<boolean>(false);
  const [inviteDialogOpen, toggleInviteDialog] = React.useState<boolean>(false);
  const [inviteServer, setInviteServer] = React.useState<string>("");
  const [inviteUsername, setInviteUsername] = React.useState<string>("");
  const [inviteRoomId, setInviteRoomId] = React.useState<string>("");
  const [roomName, setRoomName] = React.useState<string>("");
  const [usernameToInvite, setUsername] = React.useState<string>("");
  const [server, setServer] = React.useState<string>("");

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const popper_open = Boolean(anchorEl);
  const popper_id = popper_open ? "simple-popper" : undefined;

  function handlePopperClick(event: React.MouseEvent<HTMLElement>) {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  }

  function closePopper() {
    setAnchorEl(null);
  }

  function handleInviteDialogOpen(room: RoomSummary) {
    setInviteRoomId(room.roomId);
    toggleInviteDialog(true);
  }

  function handleInviteDialogClose() {
    toggleInviteDialog(false);
  }

  function invite() {
    handleInvite(inviteRoomId, inviteServer, inviteUsername);
    handleInviteDialogClose();
  }

  function handleDialogOpen() {
    toggleDialog(true);
  }

  function handleDialogClose() {
    toggleDialog(false);
  }

  function handleDrawerOpen() {
    setOpen(true);
  }

  function handleDrawerClose() {
    setOpen(false);
  }

  function handleRoomsClick() {
    toggleRooms(!roomsOpen);
  }

  function addNewRoom() {
    if (roomName && usernameToInvite && server) {
      createRoom(roomName, Backend.usernameToUserId(usernameToInvite, server));
    }
    handleDialogClose();
  }

  const renderLogout = (
    <>
      <Button color="inherit" onClick={e => handleLogout()}>
        Logout
      </Button>
    </>
  );

  const renderInviteDialog = (
    <>
      <Dialog
        open={inviteDialogOpen}
        onClose={handleInviteDialogClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Invite a new User!</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To invite someone to your roome please provide a server and
            username.
          </DialogContentText>
          <TextField
            margin="dense"
            id="server"
            label="Server"
            type="text"
            fullWidth
            onChange={(
              e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
            ) => setInviteServer(e.currentTarget.value)}
          />
          <TextField
            margin="dense"
            id="username"
            label="Username"
            type="text"
            fullWidth
            onChange={(
              e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
            ) => setInviteUsername(e.currentTarget.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleInviteDialogClose} color="primary">
            Cancel
          </Button>
          <Button onClick={invite} color="primary">
            Invite
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );

  const renderAddRoomDialog = (
    <>
      <Dialog
        open={dialogOpen}
        onClose={handleDialogClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create A New Room!</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To add a new room to your list please provide a room name and try
            inviting someone using their platform adress, which usually looks
            like this "@someone:server.domain"
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Room Name"
            type="text"
            fullWidth
            onChange={(
              e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
            ) => setRoomName(e.currentTarget.value)}
          />
          <TextField
            margin="dense"
            id="server"
            label="Server"
            type="text"
            fullWidth
            onChange={(
              e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
            ) => setServer(e.currentTarget.value)}
          />
          <TextField
            margin="dense"
            id="username"
            label="Username"
            type="text"
            fullWidth
            onChange={(
              e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
            ) => setUsername(e.currentTarget.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleDialogClose} color="primary">
            Cancel
          </Button>
          <Button onClick={addNewRoom} color="primary">
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );

  //the default button in the Drawer
  const renderDefault = (
    <List>
      <ListItem button key={"login"} onClick={handleDrawerClose}>
        <ListItemIcon key={"loginIconWrapper"}>
          <ExitToApp key={"loginIcon"} />
        </ListItemIcon>
        <ListItemText primary={"Login"} key={"loginText"} />
      </ListItem>
    </List>
  );

  //holds two buttons and a list of rooms if there are any
  const renderRooms = (
    <List>
      <ListItem
        button
        key={"addRoom"}
        onClick={handleDialogOpen}
        disabled={loadingMessages}
      >
        <ListItemIcon key={"addRoomIconWrapper"}>
          <PersonAdd key={"addRoomIcon"} />
        </ListItemIcon>
        <ListItemText primary={"Add Room"} key={"addRoomText"} />
      </ListItem>
      <ListItem
        button
        key={"rooms"}
        onClick={handleRoomsClick}
        disabled={loadingMessages}
      >
        <ListItemIcon key={"ChatIconWrapper"}>
          <Chat key={"chatIcon"} />
        </ListItemIcon>
        <ListItemText primary={"Rooms"} key={"roomsText"} />
        {roomsOpen ? (
          <ExpandLess key={"roomsIcon"} />
        ) : (
          <ExpandMore key={"roomsIcon"} />
        )}
      </ListItem>
      <Collapse
        in={roomsOpen}
        timeout="auto"
        unmountOnExit
        key={"roomCollpase"}
      >
        <Divider key={"roomsDivider"} />
        <List key={"roomList"}>
          {rooms.map((room: RoomSummary) => (
            <>
              <ListItem
                button
                className={clsx({
                  [classes.activeRoom]: room === activeRoom
                })}
                onClick={() => handleRoomChange(room)}
                key={room.roomId}
                title={room.info.title}
                disabled={loadingMessages}
              >
                <ListItemAvatar key={room.roomId + ":AvatarWrapper"}>
                  <Avatar
                    key={room.roomId + "Avatar"}
                    className={classes.avatar}
                    style={{
                      backgroundColor: stringToColor(room.info.title),
                      color: "#fff",
                      marginRight: !drawerOpen ? "50px" : "0px"
                    }}
                  >
                    {room.info.title.charAt(0)}
                  </Avatar>
                </ListItemAvatar>
                {drawerOpen && (
                  <ListItemText
                    key={room.roomId + ":Text"}
                    style={{ marginRight: "10px" }}
                    primary={
                      <Typography
                        key={room.roomId + ":Typography"}
                        variant="body1"
                        style={{
                          whiteSpace: "normal",
                          wordBreak: drawerOpen ? "break-word" : "unset",
                          maxWidth: "100%"
                        }}
                      >
                        {room.info.title}
                      </Typography>
                    }
                    disableTypography
                  />
                )}
                {drawerOpen && (
                  <ListItemSecondaryAction key={room.roomId + ":Action"}>
                    <ClickAwayListener onClickAway={() => closePopper()}>
                      <div>
                        <IconButton
                          // aria-describedby={popper_id}
                          // variant="contained"
                          onClick={handlePopperClick}
                        >
                          <MoreVertIcon />
                        </IconButton>
                        <Popper
                          style={{ zIndex: 99999 }}
                          id={popper_id}
                          open={popper_open}
                          anchorEl={anchorEl}
                          transition
                        >
                          {({ TransitionProps }) => (
                            <Fade {...TransitionProps} timeout={350}>
                              <Paper>
                                {/* <Typography className={classes.typography}>The content of the Popper.</Typography> */}
                                <List key={"optionsList"}>
                                  <ListItem
                                    button
                                    onClick={() => {
                                      closePopper();
                                      handleInviteDialogOpen(room);
                                    }}
                                    key={room.roomId + ":inviteId"}
                                    title={"Invite"}
                                    disabled={loadingMessages}
                                  >
                                    <ListItemText>
                                      <Typography
                                        style={{
                                          whiteSpace: "normal",
                                          wordBreak: drawerOpen
                                            ? "break-word"
                                            : "unset",
                                          maxWidth: "100%"
                                        }}
                                      >
                                        Invite
                                      </Typography>
                                    </ListItemText>
                                    <AddIcon
                                      key={room.roomId + ":inviteIcon"}
                                    />
                                  </ListItem>
                                  <ListItem
                                    button
                                    className={clsx({
                                      [classes.activeRoom]: room === activeRoom
                                    })}
                                    onClick={() => {
                                      closePopper();
                                      handleLeaveRoom(room);
                                    }}
                                    key={room.roomId + ":leaveId"}
                                    title={"Leave"}
                                    disabled={loadingMessages}
                                  >
                                    <ListItemText>
                                      <Typography
                                        style={{
                                          whiteSpace: "normal",
                                          wordBreak: drawerOpen
                                            ? "break-word"
                                            : "unset",
                                          maxWidth: "100%"
                                        }}
                                      >
                                        Leave
                                      </Typography>
                                    </ListItemText>
                                    <DeleteIcon key={room.roomId + ":icon"} />
                                  </ListItem>
                                </List>
                              </Paper>
                            </Fade>
                          )}
                        </Popper>
                      </div>
                    </ClickAwayListener>
                  </ListItemSecondaryAction>
                )}
              </ListItem>
              <Divider key={room.roomId + ":Divider"} />
            </>
          ))}
        </List>
      </Collapse>
    </List>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.shiftElement]: drawerOpen
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: drawerOpen
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Matrix Chat
          </Typography>
          <div className={classes.grow} />
          {loggedIn ? renderLogout : ""}
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: drawerOpen,
          [classes.drawerClose]: !drawerOpen
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: drawerOpen,
            [classes.drawerClose]: !drawerOpen
          })
        }}
        open={drawerOpen}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        {loggedIn ? renderRooms : renderDefault}
      </Drawer>
      {renderAddRoomDialog}
      {renderInviteDialog}
      <Grid
        container
        direction="column"
        justify="flex-end"
        alignItems="center"
        className={classes.chatView}
      >
        <div className={classes.appBarSpacer} />
        {children}
      </Grid>
    </div>
  );
}

//css
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    typography: {
      padding: theme.spacing(2)
    },
    root: {
      display: "flex",
      height: "100%"
    },
    avatar: {
      marginTop: 5,
      marginBottom: 5
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1
    },
    grow: {
      flexGrow: 1
    },
    form: {
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.black, 0.15),
      "&:hover": {
        backgroundColor: fade(theme.palette.common.black, 0.35)
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        marginLeft: theme.spacing(3),
        width: "auto"
      }
    },
    formIcon: {
      width: theme.spacing(7),
      height: "100%",
      position: "absolute",
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    inputRoot: {
      color: "inherit"
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create("width"),
      [theme.breakpoints.up("md")]: {
        width: 200
      }
    },
    wrapper: {
      margin: theme.spacing(1),
      position: "relative"
    },
    buttonSuccess: {
      backgroundColor: green[500],
      "&:hover": {
        backgroundColor: green[700]
      }
    },
    buttonFailure: {
      backgroundColor: red[500],
      "&:hover": {
        backgroundColor: red[700]
      }
    },
    buttonProgress: {
      color: green[500],
      position: "absolute",
      top: "50%",
      left: "50%",
      marginTop: -12,
      marginLeft: -12
    },
    topBar: {
      top: 0,
      bottom: "auto"
    },
    appBarSpacer: theme.mixins.toolbar,
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap"
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1
      }
    },
    toolbar: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: "0 8px",
      ...theme.mixins.toolbar
    },
    hide: {
      display: "none"
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    shiftElement: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    activeRoom: {
      background: "lightgray"
    },
    chatView: {
      minHeight: "100vh"
    }
  })
);
