import React from "react";
import Container from "@material-ui/core/Container";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Storage from "@material-ui/icons/Storage";
import VpnKey from "@material-ui/icons/VpnKey";
import { green, red } from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import clsx from "clsx";
import {
  CircularProgress,
  Paper,
  IconButton,
  Typography,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
  DialogActions,
  FormHelperText
} from "@material-ui/core";
import Backend from "../scripts/backend";
import { isValidWebAdress, getErrorMessage } from "../utils/utils";

/**
 * Has functions for sending texinput change to the ChatApp component
 * and various flags
 *
 * @export
 * @interface LoginFormProps
 */
export interface LoginFormProps {
  loading: boolean;
  loggedIn: boolean;
  loginFailure: boolean;
  handleUserChange: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  handleServerChange: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  handleSubmit: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  handlePwChange: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
}

//used for component styling
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: "auto"
    },
    input: {
      padding: "10px",
      display: "flex",
      alignItems: "center",
      margin: "10px"
    },
    iconButton: {
      padding: 10
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create("width"),
      [theme.breakpoints.up("md")]: {
        width: 200
      }
    },
    buttonWrapper: {
      margin: "10px",
      position: "relative"
    },
    button: {
      height: "50px"
    },
    buttonSuccess: {
      backgroundColor: green[500],
      "&:hover": {
        backgroundColor: green[700]
      }
    },
    buttonFailure: {
      backgroundColor: red[500],
      "&:hover": {
        backgroundColor: red[700]
      }
    },
    buttonProgress: {
      color: green[500],
      position: "absolute",
      top: "50%",
      left: "50%",
      marginTop: -12,
      marginLeft: -12
    },
    textfield: {
      padding: theme.spacing(1)
    }
  })
);

/**
 *
 *
 * @export
 * @param {LoginFormProps} {
 *   handleSubmit,
 *   handlePwChange,
 *   handleServerChange,
 *   handleUserChange,
 *   loading,
 *   loggedIn,
 *   loginFailure
 * }
 * @returns a Form containing three Texfields and two buttons, as well as a Dialog with a Form.
 */
export default function LoginForm({
  handleSubmit,
  handlePwChange,
  handleServerChange,
  handleUserChange,
  loading,
  loggedIn,
  loginFailure
}: LoginFormProps) {
  const classes = useStyles();

  //hooks for login form
  const [clientServer, setClientServer] = React.useState<string>("");
  const [loginFormError, setLoginFormError] = React.useState<boolean>(false);

  //hooks for registration dialog
  const [username, setUsername] = React.useState<string>("");
  const [password, setPassword] = React.useState<string>("");
  const [confirmPassword, setConfirmationPassword] = React.useState<string>("");
  const [server, setServer] = React.useState<string>("");
  //these next four hooks are used to display errors and validation problems
  const [registerLoading, setRegisterLoading] = React.useState<boolean>(false);
  const [registerSuccess, setRegisterSuccess] = React.useState<boolean>();
  const [validationError, setValidationError] = React.useState<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string | undefined>(
    undefined
  );

  //hook for opening and closing the dialog
  const [dialogOpen, toggleDialog] = React.useState<boolean>(false);
  function handleDialogOpen() {
    toggleDialog(true);
  }

  function handleDialogClose() {
    toggleDialog(false);
  }

  /**
   * Used to register at a server
   *
   */
  function register() {
    setRegisterLoading(true);
    if (
      username.length > 0 &&
      password.length > 0 &&
      server.length > 0 &&
      password === confirmPassword &&
      isValidWebAdress(server)
    ) {
      let backend = new Backend(server);
      try {
        backend.register(username, password, (error: any, data: any) => {
          setRegisterLoading(false);
          if (data) {
            setRegisterSuccess(true);
          } else if (error) {
            setRegisterSuccess(false);
            setErrorMessage(getErrorMessage(error.httpStatus, error.errcode));
            console.log(JSON.stringify(error.httpStatus, undefined, "    "));
            console.log(JSON.stringify(error.errcode, undefined, "    "));
          }
        });
      } catch (error) {
        setErrorMessage("An error occurred during the registration process.");
      }
    } else {
      setValidationError(true);
    }
    setRegisterLoading(false);
  }

  return (
    <Container maxWidth={"sm"} className={classes.root}>
      <Typography variant="h3" component="h2" gutterBottom>
        LOGIN
      </Typography>
      <Paper className={classes.input}>
        <IconButton className={classes.iconButton} tabIndex={-1}>
          <AccountCircle />
        </IconButton>
        <InputBase
          error={true}
          required={true}
          disabled={loading}
          placeholder="Username"
          inputProps={{ "aria-label": "Username" }}
          onChange={e => handleUserChange(e)}
          onFocus={e => setLoginFormError(false)}
          fullWidth
        />
      </Paper>
      <Paper className={classes.input}>
        <IconButton className={classes.iconButton} tabIndex={-1}>
          <VpnKey />
        </IconButton>
        <InputBase
          error={loginFailure}
          required={true}
          disabled={loading}
          placeholder="Password"
          inputProps={{ "aria-label": "Password" }}
          onChange={e => handlePwChange(e)}
          onFocus={e => setLoginFormError(false)}
          type={"password"}
          fullWidth
        />
      </Paper>
      <Paper className={classes.input}>
        <IconButton className={classes.iconButton} tabIndex={-1}>
          <Storage />
        </IconButton>
        <InputBase
          defaultValue={"https://"}
          required={true}
          disabled={loading}
          placeholder="Server Adress"
          inputProps={{ "aria-label": "Synapse Server Adress" }}
          onFocus={e => setLoginFormError(false)}
          onChange={e => {
            const { value } = e.currentTarget;
            setClientServer(value);
            handleServerChange(e);
          }}
          fullWidth
        />
      </Paper>
      {loginFormError && <FormHelperText>Invalid Server</FormHelperText>}
      <Grid container>
        <Grid item xs={12} sm={6}>
          <div className={classes.buttonWrapper}>
            <Button
              variant="contained"
              className={classes.button}
              color="primary"
              disabled={loading}
              onClick={handleDialogOpen}
              fullWidth
            >
              Register instead!
            </Button>
          </div>
        </Grid>
        <Grid item xs={12} sm={6}>
          <div className={classes.buttonWrapper}>
            <Button
              variant="contained"
              className={clsx(classes.button, {
                [classes.buttonFailure]: loginFailure,
                [classes.buttonSuccess]: loggedIn
              })}
              /* className={
                loggedIn && !loading ? classes.buttonSuccess : classes.button
              } */
              disabled={loading}
              onClick={e => {
                let { value } = e.currentTarget;
                let trimmedValue = value.trim();
                if (
                  isValidWebAdress(clientServer) ||
                  trimmedValue === "https://"
                ) {
                  handleSubmit(e);
                } else {
                  setLoginFormError(true);
                }
                console.log("CLicked Login");
              }}
              fullWidth
            >
              Log In
            </Button>
            {loading && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
          </div>
        </Grid>
      </Grid>
      <Dialog
        open={dialogOpen}
        onClose={handleDialogClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create A New Room!</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please provide a Username and a Password. You also have to specify a
            Server. A Server Adress can look like this:
            "https://matrix.server.domain".
          </DialogContentText>
          <Grid container>
            <Grid item xs={12} className={classes.textfield}>
              <TextField
                error={validationError}
                autoFocus
                margin="normal"
                id="username"
                label="Username"
                type="text"
                fullWidth
                onFocus={() => {
                  if (validationError) {
                    setValidationError(false);
                  }
                }}
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => setUsername(e.currentTarget.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6} className={classes.textfield}>
              <TextField
                error={validationError}
                margin="normal"
                id="password"
                label="Password"
                type="password"
                fullWidth
                onFocus={() => {
                  if (validationError) {
                    setValidationError(false);
                  }
                }}
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => setPassword(e.currentTarget.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6} className={classes.textfield}>
              <TextField
                error={validationError}
                margin="normal"
                id="pwconfirm"
                label="Confirm Password"
                type="password"
                fullWidth
                onFocus={() => {
                  if (validationError) {
                    setValidationError(false);
                  }
                }}
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => setConfirmationPassword(e.currentTarget.value)}
              />
            </Grid>
            <Grid item xs={12} className={classes.textfield}>
              <TextField
                error={validationError}
                margin="normal"
                id="server"
                label="Server Adress"
                type="server"
                fullWidth
                onFocus={() => {
                  if (validationError) {
                    setValidationError(false);
                  }
                }}
                onChange={(
                  e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
                ) => setServer(e.currentTarget.value)}
              />
              {errorMessage ? (
                <FormHelperText>{errorMessage}</FormHelperText>
              ) : (
                <></>
              )}
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button disabled={registerLoading} onClick={handleDialogClose}>
            Cancel
          </Button>
          <div className={classes.buttonWrapper}>
            <Button
              disabled={registerLoading}
              // using clsx to change className depending on the flagss
              className={clsx(classes.button, {
                [classes.buttonSuccess]: registerSuccess,
                [classes.buttonFailure]:
                  registerSuccess === undefined ? false : !registerSuccess
              })}
              color="primary"
              onClick={() => {
                register();
              }}
            >
              Submit
            </Button>
            {loading && !loggedIn && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
          </div>
        </DialogActions>
      </Dialog>
    </Container>
  );
}
