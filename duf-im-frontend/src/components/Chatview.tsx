import React from "react";
import {
  Grid,
  Paper,
  InputBase,
  IconButton,
  AppBar,
  CircularProgress,
  Fade,
  Popper,
  ClickAwayListener
} from "@material-ui/core";
import SendIcon from "@material-ui/icons/Send";
import InsertEmoticon from "@material-ui/icons/InsertEmoticon";
import { Theme, createStyles, makeStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
import EmojiPicker from "emoji-picker-react";

export interface ChatviewProps {
  onSendMessage: (message: string) => void;
  children?: React.ReactNode;
  loadingMessages: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      top: "auto",
      bottom: 0,
      padding: "5px"
    },
    inputWrapper: {
      padding: "5px"
    },
    appBarSpacer: theme.mixins.toolbar,
    input: {
      padding: "7px 7px",
      fontSize: 16
    },
    iconButton: {
      padding: 10
    },
    circularLoader: {
      color: green[500],
      margin: "auto"
    }
  })
);

/**
 * Container for Chat-Participant Information and Chat-History.
 * It only has a bottom TextField
 *
 * @export
 * @param {ChatviewProps} {
 *   onInputChange,
 *   onSendMessage,
 *   children,
 *   loadingMessages
 * }
 * @returns
 */
export default function Chatview({
  onSendMessage,
  children,
  loadingMessages
}: ChatviewProps) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  function handleClick(event: any) {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  }

  function closePopper() {
    setAnchorEl(null);
  }

  const open = Boolean(anchorEl);
  const id = open ? "simple-popper" : undefined;
  const inputRef: React.RefObject<HTMLTextAreaElement> = React.createRef<
    HTMLTextAreaElement
  >();
  return (
    <>
      {/* shows a loading screen if loading */}
      {loadingMessages ? (
        <CircularProgress className={classes.circularLoader} size={100} />
      ) : (
        <Fade
          in={!loadingMessages}
          {...(!loadingMessages ? { timeout: 1000 } : {})}
        >
          <Grid container direction={"column"}>
            {children}
          </Grid>
        </Fade>
      )}
      {/* The AppBar contains a TextField to send messages */}
      <AppBar position="sticky" color="primary" className={classes.appBar}>
        <Paper>
          <InputBase
            onKeyDown={(e: any) => {
              if (e.key === "Enter") {
                e.preventDefault();
                if (inputRef.current && inputRef.current.value) {
                  onSendMessage(e.currentTarget.value);
                }
                if (inputRef.current && inputRef.current.value) {
                  inputRef.current.value = "";
                }
              }
            }}
            rowsMax={5}
            className={classes.input}
            placeholder="CHAT!"
            fullWidth
            autoFocus
            multiline
            inputProps={{ "aria-label": "Search Google Maps" }}
            inputRef={inputRef}
            startAdornment={
              <>
                <IconButton onClick={e => handleClick(e)}>
                  <InsertEmoticon />
                </IconButton>
                <ClickAwayListener onClickAway={closePopper}>
                  <Popper
                    style={{ zIndex: 99999 }}
                    id={id}
                    open={open}
                    anchorEl={anchorEl}
                    placement="top-start"
                    modifiers={{
                      flip: {
                        enabled: true
                      },
                      preventOverflow: {
                        enabled: true,
                        boundariesElement: "viewport"
                      }
                    }}
                  >
                    <EmojiPicker
                      onEmojiClick={data => {
                        if (inputRef.current) {
                          inputRef.current.value += String.fromCodePoint(
                            parseInt("0x" + data)
                          );
                          inputRef.current.dispatchEvent(new Event("change"));
                          closePopper();
                        }
                      }}
                    />
                  </Popper>
                </ClickAwayListener>
              </>
            }
            endAdornment={
              <>
                <IconButton
                  aria-describedby={id}
                  className={classes.iconButton}
                  aria-label="Search"
                  onClick={(e: any) => {
                    if (inputRef.current && inputRef.current.value) {
                      onSendMessage(inputRef.current.value);
                      inputRef.current.value = "";
                    }
                  }}
                >
                  <SendIcon />
                </IconButton>
              </>
            }
          />
        </Paper>
      </AppBar>
    </>
  );
}
