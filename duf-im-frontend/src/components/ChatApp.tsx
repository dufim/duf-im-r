import * as React from "react";
import User from "../model/User";
import Cookies from "universal-cookie";
import Backend from "../scripts/backend";
import Message, { MessageEvent } from "../model/Message";
import Chatview from "./Chatview";
import AuthAppBar from "./AppBar";
import { RoomSummary } from "../model/Room";
import LoginForm from "./LoginForm";
import ChatHistory from "./ChatHistory";
import {
  getUserIdFromServerAndUsername,
  getUsernameFromUserId
} from "../utils/utils";

/**
 *
 *
 * @interface LoginData
 */
interface LoginData {
  username: string;
  password: string;
  server: string;
}

/**
 * Describes what state must have
 *
 * @export
 * @interface ChatAppStateProps
 */
export interface ChatAppStateProps {
  //The logged in user
  activeUser: User;
  //currently selected room
  activeRoom: RoomSummary | undefined;
  //this object is filled when the LoginForm is being edited
  loginData: LoginData;
  //the messages to display
  messages: Message[];
  //holds all loaded messages for all available rooms. key: roomId
  messagesMap: Map<string, Message[]>;
  //holds a message to be sent to a room
  textinput: string;
  //true if in login process
  loginLoading: boolean;
  //true if the room changes and state.messages have to be repopulated
  loadingMessages: boolean;
  //indicates a successful login
  loggedIn: boolean;
  //indicates a failed login attempt
  loginFailure: boolean;
  //holds all rooms the active user is in
  rooms: RoomSummary[];
}

//used to scroll to the end of the displayed messages
var historyEndRef: React.RefObject<HTMLDivElement>;

//used to initiate and reset state
const initState: ChatAppStateProps = {
  activeUser: {
    master: false,
    matrixAddress: "",
    username: "",
    accessToken: ""
  },
  activeRoom: {
    roomId: "",
    info: { title: "" }
  },
  loginData: {
    username: "",
    password: "",
    server: ""
  },
  messages: [],
  textinput: "",
  loginLoading: false,
  loggedIn: false,
  rooms: [],
  messagesMap: new Map<string, Message[]>(),
  loginFailure: false,
  loadingMessages: false
};

/**
 * A React Component that holds all major state
 *
 * @export
 * @class ChatApp
 * @extends {React.Component<any, ChatAppStateProps>}
 */
export default class ChatApp extends React.Component<any, ChatAppStateProps> {
  //for reading and writing cookies
  cookies: Cookies;
  //a client to communicate with a matrix server
  backend: Backend;

  constructor(props: any) {
    super(props);
    this.cookies = new Cookies();
    const userId: string = this.cookies.get("matrixAddress");
    const token: string = this.cookies.get("token");

    this.state = {
      activeUser: {
        master: true,
        matrixAddress: userId,
        username: userId != null ? getUsernameFromUserId(userId) : "",
        accessToken: token ? token : ""
      },
      activeRoom: initState.activeRoom,
      loggedIn: initState.loggedIn,
      loginData: initState.loginData,
      loginLoading: initState.loginLoading,
      messages: initState.messages,
      rooms: initState.rooms,
      textinput: initState.textinput,
      messagesMap: initState.messagesMap,
      loginFailure: initState.loginFailure,
      loadingMessages: initState.loadingMessages
    };

    //binding functions in this component before giving them to child components for usage
    this.handleLogout = this.handleLogout.bind(this);
    this.handleServerChange = this.handleServerChange.bind(this);
    this.handlePwChange = this.handlePwChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.login = this.login.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.messageEventHandler = this.messageEventHandler.bind(this);
    this.loadPastMessages = this.loadPastMessages.bind(this);
    this.handleRoomChange = this.handleRoomChange.bind(this);
    this.handleLeaveRoom = this.handleLeaveRoom.bind(this);
    this.handleInvite = this.handleInvite.bind(this);
    this.onNewRoom = this.onNewRoom.bind(this);
    this.createNewRoom = this.createNewRoom.bind(this);
    this.scrollBack = this.scrollBack.bind(this);
    this.backend = new Backend(
      undefined,
      token !== undefined && token.length > 0 ? token : undefined
    );
    historyEndRef = React.createRef<HTMLDivElement>();
  }

  /**
   * Triggered when the user scrolls to the top the available messages
   * and gets older messages if there are any
   *
   * @private
   * @memberof ChatApp
   */
  private scrollBack() {
    //will hold an id to scroll to
    let firstMessageInList: string;
    //will hold a MatricClient room object
    let room;

    this.setState({ loadingMessages: true }, () => {
      if (
        this.state.messages &&
        this.state.messages.length > 0 &&
        this.state.messages[0]
      ) {
        firstMessageInList = this.state.messages[0].eventId;
      }
    });

    //get the room
    if (this.state.activeRoom) {
      room = this.backend.getRoomByName(this.state.activeRoom.info.title)[0];
    }

    //load older messages for room
    this.backend.scrollBack(room, () => {
      this.setState(
        {
          loadingMessages: false
        },
        () => {
          //scroll to last scroll position before loading
          let ele = document.getElementById(firstMessageInList);
          if (ele) {
            ele.scrollIntoView();
          }
        }
      );
    });
  }

  //sets a listener for scroll top and triggers scrollBack()
  componentDidMount() {
    let that = this;
    window.onscroll = function() {
      if (window.pageYOffset === 0) {
        that.scrollBack();
      }
    };
  }

  componentWillUnmount() {
    window.onscroll = null;
  }

  /**
   * Given to the Backend to handle incoming messages
   *
   * @private
   * @param {MessageEvent} messageEvent
   * @param {boolean} addToBeginning new messages to end and old messages to top of list
   * @memberof ChatApp
   */
  private messageEventHandler(
    messageEvent: MessageEvent,
    addToBeginning: boolean
  ) {
    const { sender, content, unsigned, room_id, event_id } = messageEvent;

    //helper function to update state.messages
    const updateMessages = () => {
      let messages = this.state.messagesMap.get(room_id);
      let room = this.state.activeRoom;
      if (this.state.activeRoom && this.state.activeRoom.roomId !== room_id) {
        room = this.state.rooms.filter(room => room.roomId === room_id)[0];
      }

      if (messages && room) {
        if (addToBeginning) {
          this.setState({
            messages: messages,
            activeRoom: room
          });
        } else {
          this.setState(
            {
              messages: messages,
              activeRoom: room
            },
            this.scrollToBottom
          );
        }
      }
    };

    let userId = sender;
    let message = content.body;
    let newMessage: Message =
      userId === undefined || userId === this.state.activeUser.matrixAddress
        ? {
            owner: this.state.activeUser,
            messageBody: message,
            age: unsigned ? unsigned.age : 0,
            roomId: room_id,
            eventId: event_id
          }
        : {
            owner: {
              master: false,
              matrixAddress: userId,
              username: getUsernameFromUserId(userId)
            },
            messageBody: message,
            age: unsigned ? unsigned.age : 0,
            roomId: room_id,
            eventId: event_id
          };

    //putting new messages to end or beginning of the messages array
    if (this.state.messagesMap.has(room_id)) {
      let messagesMap = this.state.messagesMap;
      let messages = messagesMap.get(room_id);

      if (messages) {
        if (addToBeginning) {
          messages.unshift(newMessage);
        } else {
          messages.push(newMessage);
        }
        messagesMap.set(room_id, messages);
      } else {
        messagesMap.set(room_id, [newMessage]);
      }
      this.setState({
        messagesMap: messagesMap
      });
    } else {
      this.state.messagesMap.set(room_id, [newMessage]);
    }

    updateMessages();
  }

  /**
   * Loads messages for a given room. Is very similar to messageEventHandler()
   * but is only triggered once and only scrolls to bottom
   *
   * @private
   * @param {MessageEvent} messageEvent
   * @param {boolean} [addToBeginning]
   * @memberof ChatApp
   */
  private loadPastMessages(
    messageEvent: MessageEvent,
    addToBeginning?: boolean
  ) {
    const messages = this.state.messages.slice();
    let newMessages: Message[] = [];

    const { sender, content, room_id, unsigned, event_id } = messageEvent;
    const userId = sender;
    const message = content.body;

    let owner =
      userId === undefined || userId === this.state.activeUser.matrixAddress
        ? this.state.activeUser
        : {
            master: false,
            matrixAddress: userId,
            username: getUsernameFromUserId(userId)
          };

    let newMessage: Message = {
      owner: owner,
      messageBody: message,
      age: unsigned.age,
      roomId: room_id,
      eventId: event_id
    };

    newMessages.push(newMessage);

    if (this.state.messagesMap.has(room_id)) {
      let messagesMap = this.state.messagesMap;
      let messages = messagesMap.get(room_id);
      if (
        messages &&
        messages.filter(message => message.eventId === event_id).length === 0
      ) {
        if (addToBeginning) {
          messages.unshift(newMessage);
        } else {
          messages.push(newMessage);
        }
        this.state.messagesMap.set(room_id, messages);
      }
    } else {
      this.state.messagesMap.set(room_id, [newMessage]);
    }

    this.setState(
      {
        messages: addToBeginning
          ? newMessages.concat(messages)
          : messages.concat(newMessages),
        rooms: this.getRoomSummarys()
      },
      this.scrollToBottom
    );
    //this.scrollToBottom();
  }

  /**
   * Given to the LoginForm component and triggered onSubmit
   *
   * @private
   * @param {React.MouseEvent<HTMLButtonElement, MouseEvent>} e only used to prevent default action
   * @memberof ChatApp
   */
  private handleSubmit(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    e.preventDefault();

    this.setState({ loginLoading: true });
    const { username, password, server } = this.state.loginData;
    let tempServer = server;

    //if no server was specified the client will use the default server
    if (server.length > 0 && server !== "https://") {
      this.backend.setServer(server);
    } else {
      tempServer = "https://matrix.org";
    }

    //constructs userIf to login
    let userId = getUserIdFromServerAndUsername(tempServer, username);
    if (userId.length > 0) {
      this.login(userId, password);
    } else {
      this.setState({ loginLoading: false, loginFailure: true });
    }
  }

  /**
   * Sends a message to the active room if there is a message
   *
   * @private
   * @memberof ChatApp
   */
  private sendMessage(message: string) {
    if (
      this.state.activeRoom &&
      message !== "" &&
      this.state.activeRoom.roomId
    ) {
      this.backend.sendMessageToRoom(this.state.activeRoom.roomId, message);
      this.setState({ textinput: "" });
    }
  }

  /**
   * Given to LoginForm to update the TextField holding the user
   *
   * @param {(React.ChangeEvent<
   *       HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
   *     >)} e
   * @memberof ChatApp
   */
  handleUserChange(
    e: React.ChangeEvent<
      HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
    >
  ) {
    const { value } = e.currentTarget;
    e.preventDefault();
    this.setState({
      loginData: {
        username: value,
        password: this.state.loginData.password,
        server: this.state.loginData.server
      }
    });
  }

  /**
   * Given to LoginForm to update the TextField holding the password
   *
   * @param {(React.ChangeEvent<
   *       HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
   *     >)} e
   * @memberof ChatApp
   */
  handlePwChange(
    e: React.ChangeEvent<
      HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
    >
  ) {
    const { value } = e.currentTarget;
    e.preventDefault();
    this.setState({
      loginData: {
        username: this.state.loginData.username,
        password: value,
        server: this.state.loginData.server
      }
    });
  }

  /**
   * Given to LoginForm to update the TextField holding the server
   *
   * @param {(React.ChangeEvent<
   *       HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
   *     >)} e
   * @memberof ChatApp
   */
  handleServerChange(
    e: React.ChangeEvent<
      HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
    >
  ) {
    const { value } = e.currentTarget;
    e.preventDefault();
    this.setState({
      loginData: {
        username: this.state.loginData.username,
        password: this.state.loginData.password,
        server: value
      }
    });
  }

  /**
   * Used to login to a synapse server
   *
   * @private
   * @param {string} userId
   * @param {string} password
   * @returns
   * @memberof ChatApp
   */
  private login(userId: string, password: string) {
    const matrixClient = this.backend;

    if (userId.length === 0 || password.length === 0) {
      return;
    }

    //used as a callback after a login attempt
    const postLogin = (success: boolean, error?: any) => {
      //return if unsuccessful
      if (!success) {
        this.setState({ loginLoading: false, loginFailure: true });
        if (error) {
          console.log(error.error);
          console.log(error.retry_after_ms);
        }
        return;
      }

      let rooms = this.getRoomSummarys();

      //registers a messageHandler for each room
      rooms.forEach((room: RoomSummary) => {
        matrixClient.registerMessageHandlerForRoomId(
          room.roomId,
          this.messageEventHandler
        );
      });

      //initializes state
      this.setState({
        activeUser: {
          master: true,
          matrixAddress: userId,
          username: getUsernameFromUserId(userId),
          accessToken: matrixClient.access_token
        },
        loggedIn: true,
        loginFailure: false,
        rooms: rooms,
        activeRoom: rooms.length > 0 ? rooms[0] : undefined
      });

      //sets cookies
      this.cookies.set("matrixAddress", userId, { path: "/" });
      if (matrixClient.access_token) {
        this.cookies.set("token", matrixClient.access_token, { path: "/" });
      }
    };

    //tries to login using the backend
    matrixClient.loginWithPassword(userId, password, postLogin, this.onNewRoom);
  }

  /**
   * Called when a user clicked on a different room.
   * Either gets messages using the backend or swaps already loaded messages
   * into view.
   *
   * @param {RoomSummary} room
   * @memberof ChatApp
   */
  handleRoomChange(room: RoomSummary | undefined) {
    var matrixClient = this.backend;

    this.setState(
      {
        messages: [],
        loadingMessages: true
      },
      () => {
        // If Messages for a room where already loaded
        if (room) {
          if (this.state.messagesMap.has(room.roomId)) {
            let messages = this.state.messagesMap.get(room.roomId);
            if (messages) {
              this.setState(
                {
                  messages: messages,
                  activeRoom: room,
                  loadingMessages: false
                },
                this.scrollToBottom
              );
            }
            //If not then loads messages for the room
          } else {
            var clientRoom = matrixClient.getRoomByName(room.info.title)[0];
            matrixClient.getLastEvents(
              clientRoom,
              50,
              this.loadPastMessages,
              () => {
                this.setState(
                  {
                    activeRoom: room,
                    loadingMessages: false
                  },
                  this.scrollToBottom
                );
              }
            );
          }
        }
      }
    );
  }

  /**
   * Given to AuthAppBar component to leave a room
   *
   * @param {RoomSummary} room
   * @memberof ChatApp
   */
  handleLeaveRoom(room: RoomSummary) {
    this.backend.leaveRoom(room.roomId, () => {
      this.onNewRoom();
      let rooms = this.getRoomSummarys().filter(r => {
        return r.roomId !== room.roomId;
      });

      this.setState({
        rooms: rooms
      });
    });
  }

  /**
   * Given to AuthAppBar component to invite a user to a room
   *
   * @param {string} roomId
   * @param {string} server
   * @param {string} username
   * @memberof ChatApp
   */
  handleInvite(roomId: string, server: string, username: string) {
    this.backend.inviteToRoom(roomId, "@" + username + ":" + server, () => {});
  }

  /**
   * Resets local cookie and state -> returns to LoginForm
   */
  handleLogout() {
    this.cookies.remove("username");
    this.setState(initState);
  }

  /**
   * Given to Backend to update available rooms
   *
   * @memberof ChatApp
   */
  onNewRoom() {
    this.setState({
      rooms: this.getRoomSummarys()
    });
  }

  /**
   * Used to scroll to the bottom of the displayed messages
   *
   * @private
   * @memberof ChatApp
   */
  private scrollToBottom() {
    if (historyEndRef.current) {
      historyEndRef.current.scrollIntoView();
    }
  }

  /**
   *
   * @private
   * @returns a sorted list of RoomSummarys
   * @memberof ChatApp
   */
  private getRoomSummarys() {
    let rooms: RoomSummary[] = Array.from(
      this.backend.getRooms().map((room: any) => room.summary as RoomSummary)
    );
    rooms.sort((a, b) => a.info.title.localeCompare(b.info.title));
    return rooms;
  }

  /**
   * Given to AuthAppBar component to create new rooms
   *
   * @private
   * @param {string} roomName
   * @param {string} userToInvite
   * @memberof ChatApp
   */
  private createNewRoom(roomName: string, userToInvite: string) {
    if (roomName && userToInvite) {
      this.backend.createRoom(roomName, userToInvite).done(() => {
        this.setState({ rooms: this.getRoomSummarys() });
      }); // handle promise / delay
    }
  }

  /**
   * The AppBar wraps everything and the ChatView wraps the ChatHistory.
   * Depending on the state, either LoginForm or ChatView will be displaed.
   *
   * @returns
   * @memberof ChatApp
   */
  public render() {
    const { messages } = this.state;

    const {
      handleLogout,
      handlePwChange,
      handleUserChange,
      sendMessage
    } = this;

    return (
      <>
        <AuthAppBar
          handleLogout={() => handleLogout()}
          rooms={this.state.rooms}
          activeRoom={this.state.activeRoom}
          handleRoomChange={this.handleRoomChange}
          handleLeaveRoom={this.handleLeaveRoom}
          handleInvite={this.handleInvite}
          createRoom={this.createNewRoom}
          loggedIn={this.state.loggedIn}
          loadingMessages={this.state.loadingMessages}
        >
          {/* displays LoginForm as long as nobody is logged in */}
          {!this.state.loggedIn && (
            <LoginForm
              handlePwChange={e => handlePwChange(e)}
              handleSubmit={e => this.handleSubmit(e)}
              handleUserChange={e => handleUserChange(e)}
              handleServerChange={e => this.handleServerChange(e)}
              loading={this.state.loginLoading}
              loggedIn={this.state.loggedIn}
              loginFailure={this.state.loginFailure}
            />
          )}
          {this.state.loggedIn && !this.state.loginFailure && (
            <Chatview
              onSendMessage={sendMessage}
              loadingMessages={this.state.loadingMessages}
            >
              <ChatHistory history={messages} />
              <div
                style={{ float: "left", clear: "both" }}
                ref={historyEndRef}
              />
            </Chatview>
          )}
        </AuthAppBar>
      </>
    );
  }
}
