import React from "react";
import Message from "../model/Message";
import { Paper, Typography, makeStyles, Theme } from "@material-ui/core";
import { red, blue } from "@material-ui/core/colors";
import { createStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: theme.spacing(1),
      padding: theme.spacing(1)
    }
  })
);

interface Props {
  message: Message;
}

/**
 * This component represents a single message.
 * It displays the owner as well as the message itself.
 *
 * @export
 * @param {Props} { message }
 * @returns
 */
export default function MessageComponent({ message }: Props) {
  const classes = useStyles();

  return (
    <Paper
      className={classes.root}
      style={{
        backgroundColor: message.owner.master ? red[100] : blue[100]
      }}
      id={message.eventId}
    >
      <Typography variant="h6">{message.owner.username}</Typography>
      <Typography
        variant="body1"
        style={{
          whiteSpace: "normal",
          wordBreak: "break-word",
          maxWidth: "100%"
        }}
      >
        {message.messageBody}
      </Typography>
    </Paper>
  );
}
