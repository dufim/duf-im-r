/**
 * used to delay a function
 *
 * @export
 * @param {number} ms
 * @returns
 */
export function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * https://github.com/mui-org/material-ui/issues/12700
 *
 * This is not our code. Its is only used to generate a color from a given string.
 * The advantage is that it always generates the same color for the same string.
 * @param string
 */
export function stringToColor(string: string) {
  let hash = 0;
  let i;

  /* eslint-disable no-bitwise */
  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let colour = "#";

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    colour += `00${value.toString(16)}`.substr(-2);
  }
  /* eslint-enable no-bitwise */

  return colour;
}

/**
 * Tests a string using a regex if the given string is a valid adress starting with 'https://'
 *
 * @export
 * @param {string} serverAdress
 * @returns {boolean} true if valid
 */
export function isValidWebAdress(serverAdress: string): boolean {
  let trimmedString = serverAdress.trim();
  if (trimmedString.length === 0) {
    return true;
  }
  //Regex for a valid https adress
  let regexp = new RegExp(
    // eslint-disable-next-line
    /^https:\/\/?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm
  );
  return regexp.test(trimmedString);
}

/**
 *
 * @export
 * @param {string} server
 * @param {string} username
 * @returns a userId
 */
export function getUserIdFromServerAndUsername(
  server: string,
  username: string
) {
  let serverReplaced: string = server.replace("https://", "");
  return "@" + username + ":" + serverReplaced;
}

/**
 *
 * @export
 * @param {string} userId
 * @returns a username
 */
export function getUsernameFromUserId(userId: string) {
  return userId.replace("@", "").split(":")[0];
}

/**
 * Used to return certain errorMessage for specified errors in the matrix client server api
 *
 * @export
 * @param {number} httpStatusCode
 * @param {string} [type]
 * @returns {string}
 */
export function getErrorMessage(httpStatusCode: number, type?: string): string {
  console.log(httpStatusCode);
  console.log(type);

  switch (httpStatusCode) {
    case 400:
      switch (type) {
        case "M_USER_IN_USE":
          return "The Username is already in use.";
        case "M_INVALID_USERNAME":
          return "Invalid username.";
        case "M_EXCLUSIVE":
          return "You can not have this username";
      }
      break;
    case 401:
      return "This client unfortunately does not support a multistep registration process yet. Please try registering at a different server";
    case 403:
      return "You are not permitted to register at this server.";
    case 429:
      return "Too many requests.";
  }
  return "An error occurred during the registration process.";
}
