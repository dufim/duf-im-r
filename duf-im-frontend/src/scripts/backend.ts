import { MessageEvent } from "../model/Message";

/**
 * This class offers access to Matrix-JS-SDK by wrapping its functions
 * logically and not technically.
 *
 * @export
 * @class Backend
 */
export default class Backend {
 
  /**
   * 
   *
   * @type {*} matrix-js-sdk
   * @memberof Backend
   */
  sdk: any;

  /**
   *
   *
   * @type {*} MatrixClient
   * @memberof Backend
   */
  client: any;

  /**
   *
   *
   * @type {(string | null)}
   * @memberof Backend
   */
  access_token: string | null;

  /**
   * Flag signifying if the backend is ready for interaction.
   *
   * @type {boolean}
   * @memberof Backend
   */
  is_prepared: boolean;

  /**
   *Creates an instance of Backend.
   * @param {string} [server] server to use - default is matrix.org
   * @param {string} [accessToken] token - if already loggin in
   * @memberof Backend
   */
  constructor(server?: string, accessToken?: string) {
    this.sdk = require("matrix-js-sdk");
    this.client = this.sdk.createClient(server ? server : "https://matrix.org");
    this.access_token = accessToken ? accessToken : null;
    this.is_prepared = false;
  }

  /**
   * If this function returns false, the client is not logged in and, therefore,
   * the various functions of this class cannot be used.
   *
   * @private
   * @returns true if the client is ready otherwise false
   * @memberof Backend
   */
  private isReady() {
    return this.is_prepared;
  }

  /**
   * Changes the server and creates a new client for it.
   *
   * @param {string} server url of the server
   * @memberof Backend
   */
  public setServer(server: string) {
    this.client = this.sdk.createClient(server);
  }

  /**
   * Logins an existing matrix user using the user id and the corresponding password. The login is only ensured to be completed
   * when it call the postLogin function. The newRoom callback can be used to react for newly added rooms.
   * 
   * @param {string} userId matrix user id following the format @{username}:{server}
   * @param {string} password password the matrix user
   * @param {(success: boolean, error?: any) => void} [postLogin=() => {}] callback after login with success flag
   * @param {Function} newRoom callback when a new room is added by auto-joining it
   * @memberof Backend
   */
  public async loginWithPassword(
    userId: string,
    password: string,
    postLogin: (success: boolean, error?: any) => void = () => {},
    newRoom: Function
  ) {
    try {
      let that = this;
      this.client.on("RoomMember.membership", function(
        event: any,
        member: any
      ) {
        if (member.membership === "invite" && member.userId === userId) {
          that.client.joinRoom(member.roomId).done(function() {
            console.log("Auto-joined %s", member.roomId);
            newRoom();
          });
        }
      });
      await this.client.startClient({ initialSyncLimit: 10 });
      await this.client
        .login("m.login.password", { user: userId, password: password })
        .then((response: any) => {
          console.log("logged in");
          this.access_token = response.access_token;
          console.log(response);
        });
      await this.client.once("sync", function(
        state: any,
        prevState: any,
        res: any
      ) {
        console.log("LOGIN_STATE=" + state);
        console.log(res);

        that.is_prepared = true;
        postLogin(true); // workaround because once uses callbacks and not async/wait...
      }); // state will be 'PREPARED' when the client is ready to use});
      console.log("login end");
    } catch (matrixError) {
      console.table(matrixError);
      postLogin(false, matrixError);
    }
  }

  /**
   *
   * @returns rooms or empty array if client is not ready
   */
  public getRooms() {
    if (!this.isReady()) {
      console.log("check state failed");
      return [];
    }
    return this.client.getRooms();
  }

  /**
   * Registers a handler for received message for a specific room.
   *
   * @param {string} roomId the matrix room id
   * @param {Function} handler function to handle a new message; receives a message string and the toStartofTimeline variable
   * @memberof Backend
   */
  public registerMessageHandlerForRoomId(roomId: string, handler: Function) {
    this.client.on("Room.timeline", function(
      event: any,
      room: any,
      toStartOfTimeline: any
    ) {
      let message: MessageEvent = event.event;
      if (message.room_id === roomId && message.type === "m.room.message") {
        handler(message, toStartOfTimeline);
        // event.event.age is the age in miliseconds @david
      } else {
        // ignore
      }
    });
  }

  /**
   * Sends a message to a chat room.
   *
   * @param {string} roomId matrix room id
   * @param {string} message message content
   * @memberof Backend
   */
  public sendMessageToRoom(roomId: string, message: string) {
    if (!this.isReady()) {
      console.log("check state failed");
      return;
    }
    let content = {
      body: message,
      msgtype: "m.text"
    };
    this.client
      .sendEvent(roomId, "m.room.message", content, "")
      .then((res: any) => {
        console.log("message sent successfully");
      })
      .catch((err: any) => {
        console.log(err);
      });
  }


  /**
   * Sends a message to a user by trying to find a room with the same name as the user.
   *
   * @param {string} username matrix username
   * @param {string} message message content
   * @memberof Backend
   */
  public sendMessageToUsername(username: string, message: string) {
    if (!this.isReady()) {
      console.log("check state failed");
      return;
    }
    let possibleRooms = this.getRoomByName(username);
    if (possibleRooms.length > 1) {
      throw new Error("Ambiguous room name. Found: " + possibleRooms.length);
    } else if (possibleRooms.length === 0) {
      throw new Error("No room found with name matching username:" + username);
    } else {
      this.sendMessageToRoom(possibleRooms[0].roomId, message);
    }
  }

  /**
   *
   *
   * @param {string} name
   * @returns all rooms with the given name
   * @memberof Backend
   */
  public getRoomByName(name: string) {
    return this.getRooms().filter((room: any) => room.name === name);
  }

  /**
   * Loads old events for a room.
   *
   * @param {*} room
   * @param {(sender: string, message: string) => void} callback
   * @memberof Backend
   */
  public getOldEventsForRoom(
    room: any,
    callback: (sender: string, message: string) => void
  ) {
    room
      .getLiveTimeline()
      .getEvents()
      .forEach((t: any) => {
        let event = t.event;
        if (event.type === "m.room.message") {
          callback(event.sender, event.content.body);
        }
      });
  }

  /**
   * 'Scrolls back' a room. Thus, loading older events.
   *
   * @param {*} room
   * @param {Function} callback get notified if done
   * @memberof Backend
   */
  public async scrollBack(room: any, callback: Function) {
    if (room) {
      this.client.scrollback(room, 30, callback).done((room: any) => {
        callback();
      });
    }
  }

  /**
   * Loads older events in the timeline of a room. The number of events in the timeline is limited by the corresponding paramter.
   * Thus, older events are not loaded if the limit is already reached. The timeline contains all messages.
   *
   * @param {*} room The room from which events should be loaded.
   * @param {number} limit The maximun of events to load.
   * @param {(message: MessageEvent) => void} handler handler to process loaded event
   * @param {() => void} callback gets notified when done
   * @memberof Backend
   */
  public getLastEvents(
    room: any,
    limit: number,
    handler: (message: MessageEvent) => void,
    callback: () => void
  ) {
    let timeline = room.getLiveTimeline();
    this.client
      .paginateEventTimeline(timeline, { backwards: true, limit: limit })
      .then((hasNext: boolean) => {
        if (hasNext) {
          timeline.getEvents().forEach((t: any) => {
            let event: MessageEvent = t.event;
            if (event.type === "m.room.message") {
              handler(event);
            }
          });
        } else {
          console.log("No more events");
        }
        callback();
      });
  }

  /**
   * Sends a message to a user by trying to find a private room with the other user. 
   * This method fails if none or more than one such room is found.
   * 
   * @param userId matrix user id
   * @param message message content
   */
  public sendMessageToUserId(userId: string, message: string) {
    if (!this.isReady()) {
      console.log("check state failed");
      return;
    }
    let possibleRooms = this.getRooms().filter((room: any) => {
      let members = room.getJoinedMembers();
      // check if private chat
      if (members.length === 2) {
        let memberWithUserId = members.filter(
          (member: any) => member.userId === userId
        );
        if (memberWithUserId.length === 1) {
          return true;
        }
      } else {
        console.log(
          "did not find private existing room with userid: " + userId
        );
        // maybe add automatic creation of user room
      }
      return false;
    });
    if (possibleRooms.length > 1) {
      // eslint-disable-next-line
      possibleRooms.map((room: any) => {
        console.log(room);
      });
      throw new Error("Ambiguous room name. Found: " + possibleRooms.length);
    } else if (possibleRooms.length === 0) {
      throw new Error("No room found with name matching userid:" + userId);
    } else {
      this.sendMessageToRoom(possibleRooms[0].roomId, message);
    }
  }

  /**
   * Creates a new room and automatically invites the given user.
   *
   * @param {string} roomName The name of room to create.
   * @param {string} userId The user to invite.
   * @returns a promise
   * @memberof Backend
   */
  public createRoom(roomName: string, userId: string) {
    return this.client.createRoom({
      // "room_alias_name": roomName,
      visibility: "private",
      invite: [userId],
      name: roomName
      // "topic",
    }); // promise
    //Resolves: {room_id: {string}, room_alias: {string(opt)}}
  }

  /**
   * Invites a user to an existing room.
   *
   * @param {string} roomId matrix room id
   * @param {string} userId matrix user id
   * @param {() => void} callback notifies when done
   * @memberof Backend
   */
  public inviteToRoom(roomId: string, userId: string, callback: () => void) {
    this.client.invite(roomId, userId, callback);
  }

  /**
   * Leaves a room and deletes it.
   *
   * @param {string} roomId matrix room id
   * @param {() => void} callback notifies when done
   * @memberof Backend
   */
  public leaveRoom(roomId: string, callback: () => void) {
    // promise or optional callback
    this.client.leave(roomId, () => {
      this.client.forget(roomId, true, callback); // delete from existence
    });
  }

  /**
   * Registers a new user.
   *
   * @param {string} username
   * @param {string} password
   * @param {Function} callback
   * @memberof Backend
   */
  public register(username: string, password: string, callback: Function) {
    try {
      this.client.register(
        username,
        password,
        null,
        { type: "m.login.dummy" },
        null,
        null,
        callback
      );
    } catch (error) {
      console.log(error);
    }
  }

  /**
   *
   *
   * @param {string} userId
   * @returns the user corresponding to the userId
   * @memberof Backend
   */
  public getUser(userId: string) {
    return this.client.getUser(userId);
  }

  /**
   *
   *
   * @param {*} user
   * @returns true if user is active otherwise false
   * @memberof Backend
   */
  public userIsActive(user: any) {
    return user.currentlyActive;
  }

  /**
   * 
   *
   * @param {*} user
   * @returns date when user was last active
   * @memberof Backend
   */
  public userLastActive(user: any) {
    let timestamp = user.getLastModifiedTime() - user.getLastActiveTs();
    let date = new Date(timestamp);
    return date;
  }

  /**
   * 
   *
   * @param {*} user
   * @returns the user's presence.
   * @memberof Backend
   */
  public userPresence(user: any) {
    return user.presence;
  }

  /**
   *
   *
   * @param {*} user
   * @returns the user's presence status message
   * @memberof Backend
   */
  public userPresenceStatusMessage(user: any) {
    return user.userPresenceStatusMsg;
  }

  /**
   * Utility function to convert a username and server to a matrix user id.
   *
   * @static
   * @param {string} username
   * @param {string} server
   * @returns matrix user id
   * @memberof Backend
   */
  public static usernameToUserId(username: string, server: string) {
    return "@" + username + ":" + server;
  }

}
