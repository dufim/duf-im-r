/**
 * Interface for a User
 *
 * @export
 * @interface User
 */
export default interface User {
  master: boolean;
  username: string;
  matrixAddress: string;
  accessToken?: string | null;
}
