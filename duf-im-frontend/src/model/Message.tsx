import User from "./User";

/**
 * Interface describing a message object.
 *
 * @export
 * @interface Message
 */
export default interface Message {
  messageBody: string;
  owner: User;
  age: number;
  roomId: string;
  eventId: string;
  receipient?: User;
}

/**
 * Interface describing a MessageEvent of the the MatrixClient
 *
 * @export
 * @interface MessageEvent
 */
export interface MessageEvent {
  content: Content;
  event_id: string;
  origin_server_ts: number;
  sender: string;
  type: string;
  unsigned: Unsigned;
  room_id: string;
}
export interface Content {
  body: string;
  msgtype: string;
}
export interface Unsigned {
  age: number;
}
