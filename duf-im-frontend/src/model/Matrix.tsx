/**
 * Interface for Error Objects of the MatrixClient
 *
 * @export
 * @interface MatrixError
 */
export interface MatrixError {
  errcode: string;
  name: string;
  message: string;
  data: Data;
  httpStatus: number;
}

export interface Data {
  errcode: string;
  error: string;
}
