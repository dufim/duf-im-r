/**
 * An Interface describing an object that the room object of the MatrixClient has.
 *
 * @export
 * @interface RoomSummary
 */
export interface RoomSummary {
  roomId: string;
  info: { title: string };
}
