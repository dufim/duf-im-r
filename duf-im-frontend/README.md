# Matrix Client

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## 1. Install dependencies

```
//will load all dependencies
yarn
```

## 2. Start the project

```
//Starts a development build.
yarn start
```

## 3. Docker

1. Build the docker image

   ```
   docker build . -t matrix-client
   ```

   You can choose any name here, but then you'd also have to change the docker-compose.yml, too.

2. Start the container

   ```
   //Standard
   docker run -p 8080:80 matrix-client

   //Using docker-compose
   docker-compose up -d
   ```

## Optional

You can change the exposed ports if you want to

```
docker run -p port:80 matrix-client
```

or modify the docker-compose.yml

```
version: "2"
services:
  matrix:
    image: matrix-client:latest
    ports:
      - port:80
    restart: unless-stopped
```
